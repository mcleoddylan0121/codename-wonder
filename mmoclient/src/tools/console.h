/*
 * console.h
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_
#include <string>

namespace wonder {
	void print(const std::string& text);           // Call printf
	void printf(const std::string& format, ...);   // Standard print (Call timestamp)
	void println(const std::string& text);         // Call print with a "\n"

	void eprint(const std::string& text);			// Call eprintf
	void eprintf(const std::string& format, ...);	// Standard print (Call timestamp and error)
	void eprintln(const std::string& text);		// Call eprint with a "\n"

	void print_timestamp();  // Prints "[HH:MM:SS.MS] " Call it before every print
	void print_error();      // Prints "*ERROR* "       Call it before error prints
}

#endif /* CONSOLE_H_ */
