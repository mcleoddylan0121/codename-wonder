#include "simplex.h"

float dot(const int8_t* g, float x, float y) {
	return g[0] * x + g[1] * y;
}

int fastfloor(float x) {
	return x > 0 ? (int) x : (int) x - 1;
}

octave_simplex::octave_simplex(int largest_feature, float persistence, int seed) {
	this->largest_f = largest_feature;
	this->persistence = persistence;
	this->seed = seed;

	num_octs = (uint8_t) ceil(log10(largest_f) / log10(2));

	octs = new simplex[num_octs];
	freqs = new float[num_octs];
	amps = new float[num_octs];

	simplex_rand rand = simplex_rand(seed);
	for (int i = 0; i < num_octs; i++) {
		octs[i] = simplex(rand.rand_uchar());

		freqs[i] = pow(2, i);
		amps[i] = pow(persistence, num_octs - i);
	}
}

float octave_simplex::operator ()(float x, float y) {
	double result = 0;
	for (int i = 0; i < num_octs; i++)
		result += octs[i](x / freqs[i], y / freqs[i]) * amps[i];
	return result;
}

simplex::simplex(int seed) {
	for (int i = 0; i < 256; i++)
		p[i] = p_supply[i];
	simplex_rand rand = simplex_rand(seed);
	for (int i = 0; i < SWAPS; i++) {
		int swapFrom = rand.rand_uchar();
		int swapTo = rand.rand_uchar();

		uint8_t temp = p[swapFrom];
		p[swapFrom] = p[swapTo];
		p[swapTo] = temp;
	}
	for (int i = 0; i < 512; i++) {
		perm[i] = p[i & 255];
		permMod12[i] = (short) (perm[i] % 12);
	}
}

float simplex::operator ()(float xin, float yin) {
	float n0, n1, n2;
	float s = (xin + yin) * F2;
	int i = fastfloor(xin + s);
	int j = fastfloor(yin + s);
	float t = (i + j) * G2;
	float X0 = i - t;
	float Y0 = j - t;
	float x0 = xin - X0;
	float y0 = yin - Y0;
	int i1, j1;
	if (x0 > y0) {
		i1 = 1;
		j1 = 0;
	} else {
		i1 = 0;
		j1 = 1;
	}
	float x1 = x0 - i1 + G2;
	float y1 = y0 - j1 + G2;
	float x2 = x0 - 1.0 + 2.0 * G2;
	float y2 = y0 - 1.0 + 2.0 * G2;
	int ii = i & 255;
	int jj = j & 255;
	int gi0 = permMod12[ii + perm[jj]];
	int gi1 = permMod12[ii + i1 + perm[jj + j1]];
	int gi2 = permMod12[ii + 1 + perm[jj + 1]];
	float t0 = 0.5 - x0 * x0 - y0 * y0;
	if (t0 < 0)
		n0 = 0.0;
	else {
		t0 *= t0;
		n0 = t0 * t0 * dot(grad3[gi0], x0, y0);
	}
	float t1 = 0.5 - x1 * x1 - y1 * y1;
	if (t1 < 0)
		n1 = 0.0;
	else {
		t1 *= t1;
		n1 = t1 * t1 * dot(grad3[gi1], x1, y1);
	}
	float t2 = 0.5 - x2 * x2 - y2 * y2;
	if (t2 < 0)
		n2 = 0.0;
	else {
		t2 *= t2;
		n2 = t2 * t2 * dot(grad3[gi2], x2, y2);
	}
	return 70.0 * (n0 + n1 + n2);
}
