#include <cstdint>
#include <cmath>
#ifndef SIMPLEX_H_
#define SIMPLEX_H_

float dot(const int8_t* g, float x, float y);
int fastfloor(float x);
static const float F2 = 0.5 * (sqrt(3.0f) - 1.0);
static const float G2 = (3.0 - sqrt(3.0f)) / 6.0;
static const uint8_t p_supply[256] = { 151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7,
		225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75,
		0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174,
		20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83,
		111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65,
		25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196, 135, 130, 116,
		188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38,
		147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223,
		183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129,
		22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
		251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239,
		107, 49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150,
		254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156,
		180 };
static const int8_t grad3[12][3] = { { 1, 1, 0 }, { -1, 1, 0 }, { 1, -1, 0 }, { -1, -1, 0 }, { 1, 0, 1 },
		{ -1, 0, 1 }, { 1, 0, -1 }, { -1, 0, -1 }, { 0, 1, 1 }, { 0, -1, 1 }, { 0, 1, -1 }, { 0, -1,
				-1 } };
static const int SWAPS = 400;
class simplex_rand {
private:
	unsigned int seed;
	unsigned int rand() {
		seed = (seed * 1103515245U + 12345U) & 0x7fffffffU;
		return seed;
	}
public:
	simplex_rand(int seed) {
		this->seed = seed & 0x7fffffffU;
	}
	uint8_t rand_uchar() {
		return (uint8_t) rand();
	}
};
class noise_2d {
public:
	virtual float operator ()(float x, float y) = 0;
	virtual ~noise_2d() {
	}
};

class simplex: public noise_2d {
private:
	uint8_t p[256];

	uint8_t perm[512];
	uint8_t permMod12[512];
public:
	simplex(int seed);
	simplex() {
	}
	float operator ()(float x, float y);
};
class scaled_simplex: public noise_2d {
private:
	simplex simp;
	float scale;
public:
	scaled_simplex(int seed, float scale) {
		this->simp = simplex(seed);
		this->scale = scale;
	}
	float noise(float x, float y) {
		return simp(x/scale,y/scale);
	}

};
class octave_simplex: public noise_2d {
	/** Creates a new terrain generator. Call this with the same parameters to get the same terrain */
private:
	int largest_f;
	float persistence;
	int seed;
	simplex* octs;
	float* freqs;
	float* amps;
	uint8_t num_octs;

public:
	octave_simplex(int largest_feature, float persistence, int seed);
	float operator ()(float x, float y);
};
#endif /*SIMPLEX_H_*/
