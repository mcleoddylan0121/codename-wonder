/*
 * shape.h
 *
 *  Created on: Feb 1, 2015
 *      Author: Dylan
 */

#ifndef SHAPE_H_
#define SHAPE_H_
namespace wonder {

	class point;
	class shape {
	public:
		virtual float height() const = 0;
		virtual float width() const = 0;
		virtual float center_x() const = 0;
		virtual float center_y() const = 0;
		virtual point* center() = 0;
		virtual float left() const = 0;
		virtual float right() const = 0;
		virtual float top() const = 0;
		virtual float bottom() const = 0;

		virtual unsigned char get_id() const = 0;
		virtual ~shape();
	};

	class point: public shape {
	public:
		float x, y;
		float height() const;
		float width() const;
		float center_x() const;
		float center_y() const;
		point* center();
		float left() const;
		float right() const;
		float top() const;
		float bottom() const;
		void move(float x, float y);

		virtual unsigned char get_id() const {
			return 1;
		}
		point(float x, float y);
		~point();
	};

	class rect: public shape {
	private:
		float x1, y1, x2, y2;
	public:
		float height() const;
		float width() const;
		float center_x() const;
		float center_y() const;
		point* center();
		float left() const;
		float right() const;
		float top() const;
		float bottom() const;

		virtual unsigned char get_id() const {
			return 2;
		}
		rect(float x, float y, float width, float height);
		~rect();
	};

	bool check_for_collision(const shape& shape1, const shape& shape2);
}

#endif /* SHAPE_H_ */
