/*
 * shape.cpp
 *
 *  Created on: Feb 2, 2015
 *      Author: Dylan
 */
#include "shape.h"

wonder::shape::~shape() {

}

wonder::point::point(float x, float y) {
	this->x = x;
	this->y = y;
}

wonder::point::~point() {

}

float wonder::point::height() const {
	return 0;
}

float wonder::point::width() const {
	return 0;
}

float wonder::point::center_x() const {
	return x;
}

float wonder::point::center_y() const {
	return y;
}

wonder::point* wonder::point::center() {
	return this;
}

float wonder::point::left() const {
	return x;
}

float wonder::point::right() const {
	return x;
}

float wonder::point::top() const {
	return y;
}

float wonder::point::bottom() const {
	return y;
}

void wonder::point::move(float x, float y) {
	this->x += x;
	this->y += y;
}

wonder::rect::rect(float x, float y, float width, float height) {
	x1 = x - width / 2;
	x2 = x + width / 2;
	y1 = y - width / 2;
	y2 = y + width / 2;
}

wonder::rect::~rect() {

}

wonder::point* wonder::rect::center() {
	return new wonder::point((x1 + x2) / 2, (y1 + y2) / 2);
}

float wonder::rect::center_x() const {
	return (x1 + x2) / 2;
}

float wonder::rect::center_y() const {
	return (y1 + y2) / 2;
}

float wonder::rect::width() const {
	return x2 - x1;
}

float wonder::rect::height() const {
	return y2 - y1;
}

float wonder::rect::left() const {
	return x1;
}

float wonder::rect::right() const {
	return x2;
}

float wonder::rect::top() const {
	return y1;
}

float wonder::rect::bottom() const {
	return y2;
}

bool wonder::check_for_collision(const shape& shape1, const shape& shape2) {
	return false; // TODO: this
}

