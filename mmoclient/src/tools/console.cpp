/*
 * console.cpp
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */
#include <cstdio>
#include <string>
#include <stdarg.h>
#include <time.h>
#include <iterator>

#include "console.h"

void wonder::print(const std::string& text) {
	wonder::printf(text);
}

void wonder::printf(const std::string& format, ...) {
	wonder::print_timestamp();
	va_list args;
	va_start(args, format);
	vprintf(format.c_str(), args);
	va_end(args);
}

void wonder::println(const std::string& text) {
	wonder::print(text + "\n");
}

void wonder::eprint(const std::string& text) {
	wonder::eprintf(text);
}

void wonder::eprintf(const std::string& format, ...) {
	wonder::print_error();
	wonder::print_timestamp();
	va_list args;
	va_start(args, format);
	vprintf(format.c_str(), args);
	va_end(args);
}

void wonder::eprintln(const std::string& text) {
	wonder::eprint(text + "\n");
}

void wonder::print_timestamp() {
	time_t rawtime;
	time(&rawtime);
	std::string timestamp = ctime(&rawtime);
	std::printf(("[" + (timestamp.erase(timestamp.length() - 1)) + "] ").c_str());
}

void wonder::print_error() {
	std::printf("*ERROR* ");
}

