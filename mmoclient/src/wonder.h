/*
 * Game.h
 *
 *  Created on: Feb 2, 2015
 *      Author: Dylan
 */
#ifndef GAME_H_
#define GAME_H_
#include <string>
#include <iostream>
#define debug std::cout << "debugging in progress!" << std::endl
namespace wonder {
	const std::string NAME = "Codename: Wonder";
	const std::string ASSETS = "assets/";
	const float PIXEL_SIZE = 1.0;

	// This is how you singleton, right?
	class sdl_controller;
	extern sdl_controller* sdl_c;

	class gl_controller;
	extern gl_controller* gl_c;

	class game_loop;
	extern game_loop* loop;

	class keyboard;
	extern keyboard keys;

	// For simplicity's sake, even though server sided, this is stored in an instance:
	extern float delta;
	extern bool running;
}

#endif /* GAME_H_ */

