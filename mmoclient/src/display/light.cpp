/*
 * light.cpp
 *
 *  Created on: Mar 20, 2015
 *      Author: Dylan
 */

#include "light.h"
#include "camera.h"
#include "gl_controller.h"

namespace wonder {

	// The camera should use an orthographic projection
	directional_light::directional_light(int width, int height, camera* cam) {
		fbo = new frame_buffer(width, height, frame_buffer::DEPTH);
		this->cam = cam;
	}

	directional_light::~directional_light() {
		delete cam;
		delete fbo;
	}

	point_light::point_light() {
		// TODO Auto-generated constructor stub

	}

	point_light::~point_light() {
		// TODO Auto-generated destructor stub
	}

} /* namespace wonder */
