/*
 * camera.h
 *
 *  Created on: Feb 25, 2015
 *      Author: Dylan
 */

#ifndef CAMERA_H_
#define CAMERA_H_
#define GLM_FORCE_RADIANS

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
namespace wonder {
	class camera {
	private:
		// Given values
		glm::vec3 position;
		glm::vec3 camera_position;
		glm::vec3 up;
		glm::mat4 projection;

		// Inferred values
		glm::mat4 view;
		glm::mat4 view_projection;
	public:
		camera(glm::mat4 projection, glm::vec3 position, glm::vec3 camera_position, glm::vec3 up);
		~camera();

		glm::mat4 get_view();
		glm::mat4 get_projection();
		glm::mat4 get_view_projection();
		glm::vec3 get_position();
		glm::vec3 get_camera_position();
		void calculate_view();
		void calculate_view_projection();
		void recalculate();
		void set_position(float x, float y, float z);
		void move(float x, float y, float z);
		void set_camera(glm::vec3 val);
		void move_camera(glm::vec3 val);
		void rotate(float angle);
		void set_projection(glm::mat4 perspective);
	};
}

#endif /* CAMERA_H_ */
