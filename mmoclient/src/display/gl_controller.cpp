/*
 * gl_factory.cpp
 *
 *  Created on: Feb 2, 2015
 *      Author: Dylan
 */

#include "gl_controller.h"
#include "../io/io.h"
#include "../tools/console.h"
#include "../wonder.h"
#include "vbo_indexer.h"
#include "../game/game_loop.h"
#include "sdl_controller.h"
#include "scene.h"
#include "camera.h"

#include <SOIL.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <math.h>
#include <cmath>
#include <gl/glew.h>
#include <gl/gl.h>
#include "light.h"

wonder::texture::~texture() {}
wonder::gl_controller::~gl_controller() {}

wonder::program::program(std::string vertex, std::string fragment, int attribs) {
	name = load_shaders(vertex, fragment);
	layout_size = attribs;
}

GLuint wonder::program::getUniformLocation(std::string name) { return glGetUniformLocation(this->name, name.c_str()); }
wonder::program::~program() { glDeleteProgram(name); }

void wonder::program::enable_vertex_attribs()  { for(int i = 0; i<layout_size; i++) glEnableVertexAttribArray(i); }
void wonder::program::disable_vertex_attribs() { for(int i = 0; i<layout_size; i++) glDisableVertexAttribArray(i); }

wonder::frame_buffer::frame_buffer(float width, float height, int type): width(width), height(height) {
	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	glGenFramebuffers(1, &name);
	glBindFramebuffer(GL_FRAMEBUFFER, name);

	// The texture we're going to render to
	glGenTextures(1, &target);
	glBindTexture(GL_TEXTURE_2D, target);

	// Give an empty image to OpenGL ( the last "0" )
	switch(type) {
		case COLOR:
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			break;
		case DEPTH:
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
			break;
	}

	// Poor filtering. Needed !
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// Depth buffer
	if(type != DEPTH) { // If we're creating a depth texture, the depth buffer is redundant
		glGenRenderbuffers(1, &depth_buffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depth_buffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_buffer);
	}

	// Set "renderedTexture" as our colour attachement #0

	// Set the list of draw buffers.
	GLenum DrawBuffers[1];
	switch(type) {
		case COLOR:
			glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target, 0);
			DrawBuffers[0] = {GL_COLOR_ATTACHMENT0};
			glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers
			break;
		case DEPTH:
	        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, target, 0);
			glDrawBuffer(GL_NONE);
			break;
	}

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		wonder::eprintln("Framebuffer Initialized Improperly!");
}

wonder::frame_buffer::~frame_buffer() {
    glDeleteFramebuffers(1, &name);
    glDeleteTextures(1, &target);
    glDeleteRenderbuffers(1, &depth_buffer);

}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * *
 * 												 *
 * 					OpenGL 3.3					 *
 * 												 *
 * * * * * * * * * * * * * * * * * * * * * * * * *
 */

wonder::texture* wonder::gl_33_controller::get_texture(const std::string& name) {
	if (texture_dict.count(name) == 0)
		texture_dict[name] = new gl_33_texture(name);
	return texture_dict[name];
}

bool wonder::gl_33_controller::handle_error(GLenum error, const char * func, int line) {
	if (error != 0 && error != 1280) { // Don't return opengl INVALID_ENUM errors because they don't mean anything
		wonder::eprintf("<OpenGL> In %s on line %i: %s (%i)\n", func, line, gluErrorString(error),
				error);
		return true;
	}
	return false;
}

void wonder::gl_33_controller::std_render_shadowmap(
		const gl_33_image* m,
		float x, float y, float z,
		float w, float h, float d) {
	glm::mat4 model(1.0f);
	model = glm::scale(model, glm::vec3(w, h, d));
	model = glm::translate(model, glm::vec3(x, y, z));

	glm::mat4 teeemp = current_directional_light->cam->get_view_projection() * model;
	glUniformMatrix4fv(depthMVP, 1, GL_FALSE, &teeemp[0][0]);

	{
		glBindBuffer(GL_ARRAY_BUFFER, m->get_vertex_buffer());
		glVertexAttribPointer(0, // attribute 0. No particular reason for 0, but must match the layout in the shader.
							3,                  // size (triangles)
							GL_FLOAT,           // type
							GL_FALSE,           // normalized? <- for 3D shading
							0,                  // stride
							(void*) 0            // array buffer offset
		);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m->get_index_buffer());

		glDrawElements(GL_TRIANGLES, m->num_vertices(), GL_UNSIGNED_SHORT, (void*) 0);
	}

}
void wonder::gl_33_controller::std_render(
		const gl_33_image* m,
		float x, float y, float z,
		float width, float height, float depth) {
	glm::mat4 model(1.0f);
	model = glm::scale(model, glm::vec3(width, height, depth));
	model = glm::translate(model, glm::vec3(x, y, z));
    bind_texture(current_directional_light->fbo->target, 1);
    glUniform1i(shadowmaptexture, 1);

    const glm::mat4 bias(
            0.5, 0.0, 0.0, 0.0,
            0.0, 0.5, 0.0, 0.0,
            0.0, 0.0, 0.5, 0.0,
            0.5, 0.5, 0.5, 1.0
    );

	set_model  (model);
    glm::mat4 temp = bias * current_directional_light->cam->get_view_projection() * model;
    glUniformMatrix4fv(depthBiasMVP, 1, GL_FALSE, &temp[0][0]);

    glUniform3f(lightinvdir,
    		current_directional_light->cam->get_camera_position().x,
    		current_directional_light->cam->get_camera_position().y,
    		current_directional_light->cam->get_camera_position().z);

	uniform_mvp(view_projection * model);
	render(m);
}

void wonder::gl_33_controller::bind_directional_light(directional_light* light) {
	this->current_directional_light = light;
}

void wonder::gl_33_controller::set_model(glm::mat4 model) {
	glUniformMatrix4fv(this->model, 1, GL_FALSE, &model[0][0]);
}

void wonder::gl_33_controller::uniform_mvp(glm::mat4 mvp) {
	glUniformMatrix4fv(MVP, 1, GL_FALSE, &mvp[0][0]);
}


void wonder::gl_33_controller::render(scene* s) {
	//std::cout << (glGetString(GL_VERSION)) << std::endl;
	//clear the screen

	//glUniform1i(texSampler, 0);

	//suncam = ((instance*) s)->cam;

	bind_frame_buffer(screenbuffer);
	bind_program(temp);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	handle_error(glGetError(), __FUNCTION__, __LINE__);
	s->render(screenbuffer);
	handle_error(glGetError(), __FUNCTION__, __LINE__);

	unbind_frame_buffer();
	bind_program(screen_shader);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	render(screenbuffer->target);

	handle_error(glGetError(), __FUNCTION__, __LINE__);
}

wonder::image* wonder::gl_33_texture::get_image(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
	return load_image(x, y, w, h);
}

void wonder::gl_33_controller::delete_texture(texture* tex) {
	texture_dict.erase(((gl_33_texture*) tex)->name);
	delete tex;
}

wonder::gl_33_texture::gl_33_texture(const std::string& path) {
	this->name   = path;
	int channels;

	unsigned char *data = SOIL_load_image((ASSETS + "images/Tiles/ghoul_tiles.png").c_str(), &width, &height, &channels, SOIL_LOAD_AUTO);

	if (!data)
		printf("Couldn't load image %s.\n", path.c_str());
	glGenTextures(1, &tex);
	gl_c->bind_texture(tex, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	SOIL_free_image_data(data);
	gl_c->bind_texture(0, 0);

	if (!tex)
		printf("Couldn't load image: %s.\n", path.c_str());
}

wonder::gl_33_texture::~gl_33_texture() {
	glDeleteTextures(1, &tex);
	for (std::set<gl_33_image*>::iterator it = images.begin(); it != images.end(); ++it) {
		delete *it;
	}
	images.clear();
}

wonder::image* wonder::gl_33_texture::load_image(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
	float tBuf[8] = {
			((float) x) 	/ (float) this->width, ((float) y) 		/ (float) this->height,
			((float) x) 	/ (float) this->width, ((float) y + h) 	/ (float) this->height,
			((float) x + w) / (float) this->width, ((float) y + h) 	/ (float) this->height,
			((float) x + w) / (float) this->width, ((float) y) 		/ (float) this->height };
	GLuint i;
	glGenBuffers(1, &i);
	glBindBuffer(GL_ARRAY_BUFFER, i);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tBuf), tBuf, GL_STATIC_DRAW);
	//gl_33_image* im = new gl_33_square_image(i, this->tex, w, h);
	//images.insert(im);
	return nullptr;
}

/* automatically generates the index buffer */
wonder::image* wonder::gl_33_texture::load_model(std::vector<glm::vec3>& vertex_buffer,
		std::vector<glm::vec2>& texture_buffer, std::vector<glm::vec3>& normal_buffer,
		unsigned int shape) {
	std::vector<unsigned short> indices;

	std::vector<glm::vec3> vBuf2, nBuf2;
	std::vector<glm::vec2> tBuf2;

	//for(unsigned short i = 0; i<vBuf2.size(); i++) indices.push_back(i);

	indexVBO(
			vertex_buffer, texture_buffer, normal_buffer,
			indices, vBuf2, tBuf2, nBuf2);

	GLuint vBID, tBID, iBID, nBID;
	glGenBuffers(1, &vBID);
	glBindBuffer(GL_ARRAY_BUFFER, vBID);
	glBufferData(GL_ARRAY_BUFFER, vBuf2.size() * sizeof(glm::vec3), &vBuf2[0], GL_STATIC_DRAW);

	glGenBuffers(1, &tBID);
	glBindBuffer(GL_ARRAY_BUFFER, tBID);
	glBufferData(GL_ARRAY_BUFFER, tBuf2.size() * sizeof(glm::vec2), &tBuf2[0], GL_STATIC_DRAW);

	glGenBuffers(1, &nBID);
	glBindBuffer(GL_ARRAY_BUFFER, nBID);
	glBufferData(GL_ARRAY_BUFFER, nBuf2.size() * sizeof(glm::vec3), &nBuf2[0], GL_STATIC_DRAW);

	glGenBuffers(1, &iBID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0],
	GL_STATIC_DRAW);
	gl_33_model* i = new gl_33_model(tBID, tex, vBID, iBID, nBID, indices.size(), shape);
	// images.add(i) // we do not own this
	return i;
}

void wonder::gl_33_image::render(float x, float y, float z, float width, float height, float depth) const {
	((gl_33_controller*) wonder::gl_c)->std_render(this, x, y, z, width, height, depth);
}
void wonder::gl_33_model::render(float x, float y, float z, float width, float height, float depth) const {
	((gl_33_controller*) wonder::gl_c)->std_render(this, x, y, z, width, height, depth);
}

void wonder::gl_33_controller::bind_camera(camera* cam) {
	this->view_projection = cam->get_view_projection();
	glm::mat4 ViewMatrix = cam->get_view();
	glUniformMatrix4fv(view, 1, GL_FALSE, &ViewMatrix[0][0]);
	//glUniformMatrix4fv(VP, 1, GL_FALSE, &view_projection[0][0]);


	//glm::vec3 pos = cam->get_position();
	//glm::vec3 lightPos = glm::vec3(pos.x, pos.y, 3E+3f + pos.z);
	//glm::vec3 lightPos = glm::vec3(-cam->position.x, -cam->position.y, 1E+9f);
	//glUniform3f(light, lightPos.x, lightPos.y, lightPos.z);
}

void wonder::gl_33_controller::bind_program(wonder::program* p) {
	for(int i = 0; i<num_vertex_attribs; i++) glDisableVertexAttribArray(i);
	glUseProgram(p->name);
	current_program = p;
	current_program->enable_vertex_attribs();
	num_vertex_attribs = p->layout_size;
}

void wonder::gl_33_controller::bind_program_no_vertex_attribs(wonder::program* p) {
	for(int i = 0; i<num_vertex_attribs; i++) glDisableVertexAttribArray(i);
	num_vertex_attribs = 0;
	glUseProgram(p->name);
	current_program = p;
}

void wonder::gl_controller::bind_frame_buffer(frame_buffer* fbo) {
	if(fbo != current_frame_buffer) {
		current_frame_buffer = fbo;
		// Render to our framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, fbo->name);
		glViewport(0,0,fbo->width,fbo->height); // Render on the whole framebuffer, complete from the lower left corner to the upper right
	}
}

void wonder::gl_controller::unbind_frame_buffer() {
	if(current_frame_buffer) {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0,0,wonder::sdl_c->resolution_width,wonder::sdl_c->resolution_height);
		current_frame_buffer = nullptr;
	}
}

void wonder::gl_33_controller::bind_texture(GLuint tex, unsigned short texture_position) {
	glActiveTexture(GL_TEXTURE0 + texture_position);
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &current_bound_texture);
	if ((GLuint) current_bound_texture != tex) {
		glBindTexture(GL_TEXTURE_2D, tex);
		current_bound_texture = tex;
	}
}

void wonder::gl_33_controller::render_shadowmap(const gl_33_image* i) {
	bind_texture(i->tex, 1); // 1 Is the shadowmap's texture location in the shader
	glBindBuffer(GL_ARRAY_BUFFER, i->get_vertex_buffer());
	glVertexAttribPointer(0, // attribute 0. No particular reason for 0, but must match the layout in the shader.
						3,                  // size (triangles)
						GL_FLOAT,           // type
						GL_FALSE,           // normalized? <- for 3D shading
						0,                  // stride
						(void*) 0            // array buffer offset
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, i->get_index_buffer());

	glDrawElements(i->get_polygon_type(), i->num_vertices(), GL_UNSIGNED_SHORT, (void*) 0);
}
void wonder::gl_33_controller::render(const gl_33_image* i) {
	bind_texture(i->tex, 0);
	glBindBuffer(GL_ARRAY_BUFFER, i->get_vertex_buffer());
	glVertexAttribPointer(0, // attribute 0. No particular reason for 0, but must match the layout in the shader.
						3,                  // size (triangles)
						GL_FLOAT,           // type
						GL_FALSE,           // normalized? <- for 3D shading
						0,                  // stride
						(void*) 0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, i->buf);
	glVertexAttribPointer(1, // attribute 1. No particular reason for 1, but must match the layout in the shader.
						2,                  // size
						GL_FLOAT,           // type
						GL_FALSE,           // normalized? <- for 3D shading
						0,                  // stride
						(void*) 0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, ((gl_33_model*)i)->get_normal_buffer());
	glVertexAttribPointer(2, // attribute 1. No particular reason for 1, but must match the layout in the shader.
						3,                  // size
						GL_FLOAT,           // type
						GL_TRUE,           // normalized? <- for 3D shading
						0,                  // stride
						(void*) 0            // array buffer offset
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, i->get_index_buffer());

	glDrawElements(i->get_polygon_type(), i->num_vertices(), GL_UNSIGNED_SHORT, (void*) 0);
}

#include <time.h>
void wonder::gl_33_controller::render(GLuint tex) {
	bind_texture(tex, 0);
	glBindBuffer(GL_ARRAY_BUFFER, screenvBID);
	glVertexAttribPointer(
						0, // attribute 0. No particular reason for 1, but must match the layout in the shader.
						3,                  // size
						GL_FLOAT,           // type
						GL_FALSE,           // normalized? <- for 3D shading
						0,                  // stride
						(void*) 0            // array buffer offset
	);

	glDrawArrays(GL_TRIANGLES, 0, 6);
}

wonder::gl_33_controller::gl_33_controller() {
	glewExperimental = GL_TRUE;									// OpenGL 3.3 needs this to function
	handle_error(glewInit(), __FUNCTION__, __LINE__);			// Initialize GLEW
	wonder::sdl_c->check_for_error(__FUNCTION__, __LINE__);		// Make sure we didn't break anything
	handle_error(glGetError(), __FUNCTION__, __LINE__);			// It's okay if this returns "invalid enumerant"

	/* Begin various initialization stuff */

	// I AM THE DEVIL
	glClearColor(.666f, .666f, .666f, .666f);

	//glClearColor(0.f, 0.f, 0.f, 0.f);
	glEnable(GL_DEPTH_TEST); // Enable depth test
	glDepthFunc(GL_LEQUAL); // Accept fragment if it closer to the camera than the former one
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND); // Enable blending
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// Cull triangles which normal is not towards the camera (I.E. Don't show walls we can't see)
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	/* End various initialization stuff */

	temp = new program("shaders/temp.vs", "shaders/temp.fs", 3);

	MVP = temp->getUniformLocation("MVP");
	depthBiasMVP = temp->getUniformLocation("DepthBiasMVP");
	texSampler = temp->getUniformLocation("myTextureSampler");
	shadowmaptexture = temp->getUniformLocation("shadowMap");
	model = temp->getUniformLocation("M");
	view = temp->getUniformLocation("V");
	lightinvdir = temp->getUniformLocation("LightInvDirection_worldspace");
	bind_program_no_vertex_attribs(temp);
	glUniform1i(texSampler, 0);
	glUniform1i(shadowmaptexture, 1);

	screen_shader = new program("shaders/testest.vs", "shaders/testest.fs", 1);

	screenshadertextureID = screen_shader->getUniformLocation("renderedTexture");
	bind_program_no_vertex_attribs(screen_shader);
	glUniform1i(screenshadertextureID, 0);

	directional_light_shader = new program("shaders/shadowshader.vs", "shaders/shadowshader.fs", 1);
	depthMVP = directional_light_shader->getUniformLocation("depthMVP");

	handle_error(glGetError(), __FUNCTION__, __LINE__);

	glGenVertexArrays(1, &vAID);
	glBindVertexArray(vAID);

	glGenBuffers(1, &gl_33_square_image::vBID);
	glBindBuffer(GL_ARRAY_BUFFER, gl_33_square_image::vBID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(gl_33_square_image::vBuf), gl_33_square_image::vBuf,
	GL_STATIC_DRAW);

	glGenBuffers(1, &tBID);
	glBindBuffer(GL_ARRAY_BUFFER, tBID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tBuf), tBuf, GL_STATIC_DRAW);

	glGenBuffers(1, &gl_33_square_image::iBID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_33_square_image::iBID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(gl_33_square_image::iBuf),
			gl_33_square_image::iBuf, GL_STATIC_DRAW);

	glGenBuffers(1, &screenvBID);
	glBindBuffer(GL_ARRAY_BUFFER, screenvBID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(screen), screen, GL_STATIC_DRAW);

	screenbuffer = new frame_buffer(wonder::sdl_c->width, wonder::sdl_c->height, frame_buffer::COLOR);

	handle_error(glGetError(), __FUNCTION__, __LINE__);
}

wonder::gl_33_controller::~gl_33_controller() {
	for (auto it = texture_dict.begin(); it != texture_dict.end(); ++it) {
		// wipe the buffer stored on the GPU, skillfully avoiding memory leaks
		delete it->second;
	}
	texture_dict.clear();
	glDeleteBuffers(1, &gl_33_square_image::vBID);
	glDeleteBuffers(1, &tBID);
	glDeleteBuffers(1, &gl_33_square_image::iBID);
	glDeleteBuffers(1, &screenvBID);
	glDeleteVertexArrays(1, &vAID);

	delete screenbuffer;
	delete directional_light_shader;

	delete temp;
	delete screen_shader;
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * *
 * 												 *
 * 					End OpenGL 3.3				 *
 * 												 *
 * * * * * * * * * * * * * * * * * * * * * * * * *
 */
