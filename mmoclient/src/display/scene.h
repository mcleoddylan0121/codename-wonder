/*
 * scene.h
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */

#ifndef SCENE_H_
#define SCENE_H_
namespace wonder {
	class frame_buffer;

	/* The current thing running. Most likely an instance or the main menu. */
	class scene {
	public:
		/* render everything onscreen */
		virtual void render(frame_buffer* to) = 0;
		virtual void tick() = 0;
		scene();
		virtual ~scene();
	};
}

#endif /* SCENE_H_ */
