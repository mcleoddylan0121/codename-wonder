/*
 * sdl_controller.cpp
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */

#include "sdl_controller.h"
#include "../wonder.h"
#include "../game/game_loop.h"
#include "../tools/console.h"
#include "../io/keyboard.h"
#include <gl/glew.h>
#include <gl/gl.h>
#include <string>

wonder::sdl_controller::sdl_controller(int width, int height, bool full_screen) {
	this->width = width;
	this->height = height;

	// Create window
	// Initialize and show window
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		wonder::eprintf("SDL could not be initialized: %s\n", SDL_GetError());
		return;
	}

	// Use the core profile
	// SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	// Use OpenGL 3.1
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

	// Turn on double buffering
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	window = SDL_CreateWindow(wonder::NAME.c_str(), SDL_WINDOWPOS_CENTERED,
	SDL_WINDOWPOS_CENTERED, width, height,
			SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
					| (full_screen ? SDL_WINDOW_MAXIMIZED | SDL_WINDOW_BORDERLESS : 0));

	if (!window)
		wonder::eprintf("Couldn't create windows: %s\n", SDL_GetError());

	if(full_screen) {
		SDL_DisplayMode display;
		if(SDL_GetDesktopDisplayMode(0, &display) == 0) {
			resolution_width = display.w;
			resolution_height = display.h;
		} else {
			wonder::eprintf("Error getting SDL_DesktopDisplayMode!");
			resolution_width  = width;
			resolution_height = height;
		}
	}
	else {
		resolution_width  = width;
		resolution_height = height;
	}

	check_for_error(__FUNCTION__, __LINE__);	// Check for errors
	glContext = SDL_GL_CreateContext(window);	// Attach an OpenGL context to our window
	check_for_error(__FUNCTION__, __LINE__);	// Check for errors again; we're paranoid

	SDL_GL_MakeCurrent(window, glContext);		// Forgot to make the context current too :<

	SDL_GL_SetSwapInterval(0);				// Swap buffers according to moniter's refresh rate
}

wonder::sdl_controller::~sdl_controller() {
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

bool wonder::sdl_controller::check_for_error(const char * func, int line) {
	const char * error = SDL_GetError();
	if (*error /* First character */!= '\0') {
		wonder::eprintf("<SDL> In %s on line %i: %s\n", func, line, error);

		SDL_ClearError();
		return true;
	}

	return false;
}

void wonder::sdl_controller::swap_buffer() {
	SDL_GL_SwapWindow(window);
}

void wonder::sdl_controller::sleep(long time) {
	SDL_Delay(time);
}

#include <iostream>
void wonder::sdl_controller::handle_events() {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
			case SDL_QUIT:
				wonder::loop->kill();
				break;
			case SDL_KEYDOWN:
				wonder::keys.set_key(event.key.keysym.sym, true);
				break;
			case SDL_KEYUP:
				wonder::keys.set_key(event.key.keysym.sym, false);
				break;
			default:
				break;
		}
	}
}

