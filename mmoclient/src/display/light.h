/*
 * light.h
 *
 *  Created on: Mar 20, 2015
 *      Author: Dylan
 */

#ifndef LIGHT_H_
#define LIGHT_H_
#include "gl_controller.h"
namespace wonder {

	class camera;
	class image;

	extern program* directional_light_shader;
	class directional_light {
	public:
		camera* cam;
		frame_buffer* fbo;
		directional_light(int width, int height, camera* cam);
		virtual ~directional_light();
		void render(image* i,
				float x, float y, float z,
				float w, float h, float d);
	};

	class point_light {
	public:
		point_light();
		virtual ~point_light();
	};

} /* namespace wonder */
#endif /* LIGHT_H_ */

