/*
 * gl_factory.h
 *
 *  Created on: Feb 2, 2015
 *      Author: Dylan
 */

#ifndef GL_CONTROLLER_H_
#define GL_CONTROLLER_H_
#define GLM_FORCE_RADIANS

#include "image.h"

#include <set>
#include <unordered_map>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <cstdint>

namespace wonder {

	class camera;
	class scene;
	class directional_light;
	class point_light;

	class program { // shader program
	public:
		void enable_vertex_attribs();
		void disable_vertex_attribs();

		program(std::string vertex, std::string fragment, int attribs);
		virtual ~program();

		GLuint getUniformLocation(std::string name);
		GLuint name;
		unsigned short layout_size;
	};

	class frame_buffer {
	public:
		enum { COLOR, DEPTH };
		GLuint name; // Name of this buffer
		GLuint target; // Target to output texture to
		GLuint depth_buffer;
		float width, height;
		frame_buffer(float width, float height, int type);
		virtual ~frame_buffer();
	};

	class texture {
	private:
		virtual image* load_image(uint16_t x, uint16_t y, uint16_t w, uint16_t h) = 0;
	public:
		int width, height;
		virtual ~texture();
		virtual image* get_image(uint16_t x, uint16_t y, uint16_t w, uint16_t h) = 0;

		virtual image* load_model(
				std::vector<glm::vec3>& vertex_buffer,
				std::vector<glm::vec2>& texture_buffer,
				std::vector<glm::vec3>& normal_buffer,
				unsigned int shape) = 0;
	};

	class gl_controller {
	public:
		frame_buffer* current_frame_buffer;
		GLfloat screen[18] = {
				-1.f, -1.f, 0.f,
				 1.f, -1.f, 0.f,
				 1.f,  1.f, 0.f,
				 1.f,  1.f, 0.f,
				-1.f,  1.f, 0.f,
				-1.f, -1.f, 0.f
		};
		virtual void render(scene* s) = 0;
		virtual ~gl_controller();
		virtual texture* get_texture(const std::string& tex) = 0;
		/* safely delete the texture. we wouldn't want anything to happen to it, right? */
		virtual void delete_texture(texture* tex) = 0;

		virtual void bind_texture(GLuint tex, unsigned short position) = 0;
		virtual void bind_camera(camera* cam) = 0;
		virtual void bind_frame_buffer(frame_buffer* fbo);
		virtual void unbind_frame_buffer();
		virtual void bind_program(wonder::program* p) = 0;
		virtual void bind_directional_light(directional_light* light) = 0;
		virtual void bind_program_no_vertex_attribs(wonder::program* p) = 0;
	};

	class gl_33_texture: public texture {
	private:
		GLuint tex;
		image* load_image(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
	public:
		std::set<gl_33_image*> images;
		std::string name;
		gl_33_texture(const std::string& filename);
		~gl_33_texture();
		image* get_image(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
		image* load_model(
				std::vector<glm::vec3>& vertex_buffer,
				std::vector<glm::vec2>& texture_buffer,
				std::vector<glm::vec3>& normal_buffer,
				unsigned int poly_type);
	};

	class gl_33_controller: public gl_controller {
	private:
		int num_vertex_attribs = 0;
		program* current_program;

		program* screen_shader;
		GLuint screenshadertextureID, screenvBID;

		directional_light* current_directional_light;
		GLuint depthMVP;

		glm::mat4 view_projection;
		GLint current_bound_texture;
		std::unordered_map<std::string, gl_33_texture*> texture_dict;
		/* Shader program, Vertex Buffer ID, Index Buffer ID, Texture Buffer ID, Vertex Array ID,
		 *	MVP matrix uniform location, texSampler uniform location, layer uniform location
		 */
		GLuint tBID, vAID, MVP, texSampler, layer, shadowmaptexture, depthBiasMVP;
		GLuint view, model, light, VP, lightinvdir;

		const GLfloat tBuf[8] = {				// The texture buffer for an entire texture.
				0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f };

		void render(const gl_33_image* i);
		void render_shadowmap(const gl_33_image* m);
		void calculate_projection();
	public:
		frame_buffer* screenbuffer;
		program* temp;
		gl_33_controller();
		~gl_33_controller();
		bool handle_error(GLenum error, const char * func, int line);
		void render(wonder::scene* s);
		texture* get_texture(const std::string& tex);
		void uniform_mvp(glm::mat4 mvp);

		void std_render(const gl_33_image* i,
				float x, 	 float y, 	   float z,
				float width, float height, float depth);
		void std_render_shadowmap(
				const gl_33_image* i,
				float x, 	 float y, 	   float z,
				float width, float height, float depth);
		void render(GLuint tex); // Render the entire texture, because reasons.

		void delete_texture(texture* tex);

		void bind_directional_light(directional_light* light);
		void bind_camera(camera* cam);

		void bind_texture(GLuint tex, unsigned short texture_position);
		void bind_program(wonder::program* p);
		void bind_program_no_vertex_attribs(wonder::program* p);
		void set_model(glm::mat4 m);
	};
} /* namespace wonder */

#endif /* GL_CONTROLLER_H_ */
