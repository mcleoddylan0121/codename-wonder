/*
 * vbo_indexer.cpp
 *
 *  Created on: Feb 28, 2015
 *      Author: Dylan
 */

#include "vbo_indexer.h"

#include <map>
#include <string.h> // for memcmp

struct vertex {
public:
	glm::vec3 vert;
	glm::vec3 norm;
	glm::vec2 UV;

	// mmm look at that class-defined boost goodness
	vertex(glm::vec3 vert, glm::vec3 norm, glm::vec2 UV) :
		vert(vert), norm(norm), UV(UV) {}
};

inline bool operator ==(const vertex& lhs, const vertex& rhs) {
	return memcmp((void*) &lhs, (void*) &rhs, sizeof(vertex)) == 0;
}
inline bool operator <(const vertex& lhs, const vertex& rhs) {
	return memcmp((void*) &lhs, (void*) &rhs, sizeof(vertex)) > 0;
	//return less(vert.z,v.vert.z);
}
inline bool operator!=(const vertex& lhs, const vertex& rhs){return !operator == (lhs,rhs);}
inline bool operator> (const vertex& lhs, const vertex& rhs){ return  operator < (rhs,lhs);}
inline bool operator<=(const vertex& lhs, const vertex& rhs){ return !operator > (lhs,rhs);}
inline bool operator>=(const vertex& lhs, const vertex& rhs){ return !operator < (lhs,rhs);}

/*
 * Index our VBO, send it through here before giving it
 * to the graphics card, so we don't have duplicated
 * vertices
 */

#include<iostream>
void indexVBO(
        std::vector<glm::vec3> & in_vertices,
        std::vector<glm::vec2> & in_uvs,
        std::vector<glm::vec3> & in_normals,

        std::vector<unsigned short> & out_indices,
        std::vector<glm::vec3> & out_vertices,
        std::vector<glm::vec2> & out_uvs,
        std::vector<glm::vec3> & out_normals
			) {
	// maps vertex->associated index
	std::map<vertex, unsigned short> vertices;
    // For each input vertex
    for ( unsigned int i=0; i<in_vertices.size(); i++ ) {
    	vertex v(in_vertices[i],in_normals[i],in_uvs[i]);
    	auto found = vertices.find(v);

    	if(found != vertices.end()) { // Great Scott, we've found him!
        	out_indices.push_back(found->second); // Push back the index
    	}
    	else {
    		out_vertices.push_back(v.vert);
    		out_uvs.push_back(v.UV);
    		out_normals.push_back(v.norm);
    		unsigned short index = out_vertices.size() - 1U;
    		out_indices.push_back(index);
    		vertices[v] = index;
    	}
    }
}




