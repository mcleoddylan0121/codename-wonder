/*
 * image.h
 *
 *  Created on: Feb 22, 2015
 *      Author: Dylan
 */

#ifndef IMAGE_H_
#define IMAGE_H_

#include <gl/glew.h>
#include <gl/gl.h>
#include <glm/glm.hpp>

namespace wonder {

	class display_object {
	public:
		virtual ~display_object() {
		}
		void render(float x, float y, float w, float h) const {
			render(x, y, 0, w, h, 1);
		}
		// render it elevated
		virtual void render(float x, float y, float z, float width, float height,
				float depth) const = 0;
	};

	class image: public display_object {
	public:
		virtual ~image() {
		}
		void render(float x, float y, float w, float h) const {
			render(x, y, 0, w, h, 1);
		}
		virtual void render(float x, float y, float z, float width, float height,
				float depth) const = 0;
		virtual unsigned int get_polygon_type() const = 0;
		void render_shadowmap(float x, float y, float w, float h) const {
			render_shadowmap(x,y,0,w,h,1);
		}
		virtual void render_shadowmap(float x, float y, float z, float width, float height, float depth) const = 0;
	};

	class gl_33_image: public image {
	public:
		GLuint buf, tex;
		gl_33_image(GLuint buf, GLuint tex);

		/* don't do this unless you own the image! */
		virtual ~gl_33_image();
		virtual void render(float x, float y, float z, float width, float height, float depth) const;

		/* TODO: temporary */
		bool operator <(const gl_33_image& i) const {
			return buf < i.buf;
		}

		virtual unsigned int num_vertices() const = 0;
		virtual GLuint get_index_buffer() const = 0;
		virtual GLuint get_vertex_buffer() const = 0;
		virtual unsigned int get_polygon_type() const = 0;
		virtual void render_shadowmap(float x, float y, float z, float width, float height, float depth) const = 0;
	};

	class gl_33_square_image: public gl_33_image {
	private:
		uint16_t width, height;
		public:
		static GLushort iBuf[6];
		static GLfloat vBuf[12];

		static GLuint vBID;
		static GLuint iBID;

		gl_33_square_image(GLuint texture_buffer, GLuint texture, uint16_t w, uint16_t h);

		unsigned int num_vertices() const;
		GLuint get_index_buffer() const;
		GLuint get_vertex_buffer() const;
		virtual unsigned int get_polygon_type() const;

		//virtual void render_shadowmap(float x, float y, float z, float width, float height, float depth) const;
	};

	class gl_33_model: public gl_33_image {
	private:
		GLuint vertex_buffer, index_buffer, normal_buffer;
		unsigned int vertices, polygon_type;

	public:
		gl_33_model(
						GLuint texture_buffer, GLuint texture, GLuint vertex_buffer,
						GLuint index_buffer, GLuint normal_buffer, unsigned int vertices);
		gl_33_model(
				GLuint texture_buffer, GLuint texture, GLuint vertex_buffer,
				GLuint index_buffer, GLuint normal_buffer, unsigned int vertices,
				unsigned int polygon_type);

		virtual ~gl_33_model();
		unsigned int num_vertices() const;
		void render(float x, float y, float z, float width, float height, float depth) const;

		unsigned int get_polygon_type() const;
		GLuint get_index_buffer() const;
		GLuint get_vertex_buffer() const;
		GLuint get_normal_buffer() const;

		void render_shadowmap(float x, float y, float z, float width, float height, float depth) const;
	};
}

#endif /* IMAGE_H_ */
