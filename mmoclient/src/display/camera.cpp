/*
 * camera.cpp
 *
 *  Created on: Feb 25, 2015
 *      Author: Dylan
 */
#include "camera.h"
#include <glm/ext.hpp>

wonder::camera::~camera() {}

void wonder::camera::set_position(float x, float y, float z) {
	position = glm::vec3(x, y, z);
	recalculate();
}

void wonder::camera::move(float x, float y, float z) {
	position += glm::vec3(x, y, z);
	recalculate();
}

void wonder::camera::set_projection(glm::mat4 m) {
	this->projection = m;
	recalculate();
}

glm::mat4 wonder::camera::get_projection() { return projection; }
glm::mat4 wonder::camera::get_view() { return view; }
glm::mat4 wonder::camera::get_view_projection() { return view_projection; }
glm::vec3 wonder::camera::get_position() { return position; }
glm::vec3 wonder::camera::get_camera_position() { return camera_position; }

void wonder::camera::calculate_view() {
	view = glm::lookAt(
			camera_position,
			glm::vec3(0.f, 0.f, 0.f),
			up);
}

void wonder::camera::calculate_view_projection() {
	view_projection = glm::translate(projection * view, -position);
}

wonder::camera::camera(glm::mat4 projection, glm::vec3 position, glm::vec3 camera_position, glm::vec3 up) {
	this->projection = projection;
	this->position = position;
	this->camera_position = camera_position;
	this->up = up;
	recalculate();
}

void wonder::camera::recalculate() {
	//recalculating...
	calculate_view();
	calculate_view_projection();
}


void wonder::camera::set_camera(glm::vec3 val) {
	camera_position = val;
	recalculate();
}
void wonder::camera::move_camera(glm::vec3 val) {
	camera_position += val;
	recalculate();
}

