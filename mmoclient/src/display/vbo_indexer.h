/*
 * vbo_indexer.h
 *
 *  Created on: Feb 28, 2015
 *      Author: Dylan
 */

#ifndef VBO_INDEXER_H_
#define VBO_INDEXER_H_
#include <glm/glm.hpp>
#include <vector>

void indexVBO(
        std::vector<glm::vec3> & in_vertices,
        std::vector<glm::vec2> & in_uvs,
        std::vector<glm::vec3> & in_normals,

        std::vector<unsigned short> & out_indices,
        std::vector<glm::vec3> & out_vertices,
        std::vector<glm::vec2> & out_uvs,
        std::vector<glm::vec3> & out_normals
);

#endif /* VBO_INDEXER_H_ */
