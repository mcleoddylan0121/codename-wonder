/*
 * sdl_controller.h
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */

#ifndef SDL_CONTROLLER_H_
#define SDL_CONTROLLER_H_
#include <SDL.h>

namespace wonder {
	class sdl_controller {
	private:
		SDL_Window* window;
		SDL_GLContext glContext;

	public:
		int width, height;
		int resolution_width, resolution_height;

		sdl_controller(int width, int height, bool fullScreen);
		virtual ~sdl_controller();

		bool check_for_error(const char * func, int line);

		void swap_buffer();

		void sleep(long ms);
		void handle_events();
	};
}

#endif /* SDL_CONTROLLER_H_ */
