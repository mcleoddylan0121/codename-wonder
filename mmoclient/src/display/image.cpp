/*
 * image.cpp
 *
 *  Created on: Feb 28, 2015
 *      Author: Dylan
 */
#include "../wonder.h"
#include "image.h"
#include "gl/glew.h"
#include "gl/gl.h"
#include "gl_controller.h"
#include "light.h"

GLuint wonder::gl_33_square_image::vBID = 0;
GLuint wonder::gl_33_square_image::iBID = 0;
GLfloat wonder::gl_33_square_image::vBuf[12] = { // Vertices of the square we're going to render for each sprite
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 0.0f, 0.0f };
GLushort wonder::gl_33_square_image::iBuf[6] = { // Indices of the 2 triangles that make the square (they correspond to vBuf)
		0, 1, 2,
		2, 3, 0 };

wonder::gl_33_image::gl_33_image(GLuint buf, GLuint tex) {
	this->buf = buf;
	this->tex = tex;
}

unsigned int wonder::gl_33_square_image::get_polygon_type() const {
	return GL_TRIANGLES;
}

wonder::gl_33_square_image::gl_33_square_image(GLuint buf, GLuint tex, uint16_t w, uint16_t h) :
		gl_33_image(buf, tex) {
	this->width = w;
	this->height = h;
}

wonder::gl_33_model::gl_33_model(
		GLuint texture_buffer, GLuint texture,
		GLuint vertex_buffer, GLuint index_buffer,
		GLuint normal_buffer, unsigned int vertices) :
		gl_33_model(
				texture_buffer, texture, vertex_buffer,
				index_buffer, normal_buffer, vertices,
				GL_TRIANGLES) {
}

wonder::gl_33_model::gl_33_model(
			GLuint texture_buffer, GLuint texture, GLuint vertex_buffer,
			GLuint index_buffer, GLuint normal_buffer, unsigned int vertices,
			unsigned int polygon_type) : gl_33_image(texture_buffer, texture) {
	this->vertex_buffer = vertex_buffer;
	this->index_buffer = index_buffer;
	this->normal_buffer = normal_buffer;
	this->vertices = vertices;
	this->polygon_type = polygon_type;
}

wonder::gl_33_model::~gl_33_model() {
	glDeleteBuffers(1, &vertex_buffer);
	glDeleteBuffers(1, &index_buffer);
	glDeleteBuffers(1, &normal_buffer);
}

unsigned int wonder::gl_33_model::get_polygon_type() const { return polygon_type; }

GLuint wonder::gl_33_square_image::get_index_buffer() const {	return iBID; }
GLuint wonder::gl_33_square_image::get_vertex_buffer() const {	return vBID; }
GLuint wonder::gl_33_model::get_index_buffer() const {	return index_buffer; }
GLuint wonder::gl_33_model::get_vertex_buffer() const {	return vertex_buffer;}
GLuint wonder::gl_33_model::get_normal_buffer() const {	return normal_buffer;}

wonder::gl_33_image::~gl_33_image() {
	glDeleteBuffers(1, &buf);
}
unsigned int wonder::gl_33_square_image::num_vertices() const {
	return 2 * 3; // 2 triangles
}

unsigned int wonder::gl_33_model::num_vertices() const {
	return vertices;
}
void wonder::gl_33_model::render_shadowmap(float x, float y, float z, float width, float height, float depth) const {
	((gl_33_controller*)wonder::gl_c)->std_render_shadowmap(this,  x, y, z, width, height, depth);
}

