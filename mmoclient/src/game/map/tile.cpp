/*
 * tile.cpp
 *
 *  Created on: Feb 22, 2015
 *      Author: Dylan
 */
#include "tile.h"
#include "map.h"
#include "../../display/gl_controller.h"
#include <cstdlib>
#include <iostream>

wonder::ground_tile::ground_tile(uint16_t sx, uint16_t sy, texture* tex) :
		tile(tex) {
	this->sx = ((float) sx) / tex->width;
	this->sy = ((float) sy) / tex->height;
}
wonder::wall::wall(uint16_t side_sx, uint16_t side_sy, uint16_t top_sx, uint16_t top_sy, texture* tex) : tile(tex) {
	this->side_sx = ((float) side_sx) / tex->width;
	this->side_sy = ((float) side_sy) / tex->height;
	this->top_sx = ((float) top_sx) / tex->width;
	this->top_sy = ((float) top_sy) / tex->height;
}

wonder::tile::tile(texture* tex) { this->tex = tex; }
wonder::ground_tile_wrapper::ground_tile_wrapper(ground_tile* t) { this->tile = t; }

wonder::wall::~wall() {}
wonder::ground_tile::~ground_tile() {}
wonder::tile::~tile() {}
wonder::tile_wrapper::~tile_wrapper() {}
wonder::ground_tile_wrapper::~ground_tile_wrapper() {}

wonder::tile* wonder::tile_set::operator [](const std::string& name) { return tiles[name]; }
void wonder::tile_set::load_tile(const std::string& name, tile* t) 	 { tiles[name] = t;	   }

wonder::tile_set::tile_set(const std::string& json) {
	tex = wonder::gl_c->get_texture("tiles");
	load_tile("tall grass", new ground_tile(256, 0, tex));
	load_tile("grass", new ground_tile(288, 0, tex));
	load_tile("dirt", new ground_tile(320, 0, tex));
	load_tile("rocks", new ground_tile(352, 0, tex));
	load_tile("snow", new ground_tile(384, 0, tex));
	load_tile("water", new ground_tile(96, 64, tex));
	load_tile("deep water", new ground_tile(160, 96, tex));
}

void wonder::ground_tile::get_UV(std::vector<glm::vec2> * out) {
	out->push_back(glm::vec2(sx, sy));
	out->push_back(glm::vec2(sx, sy + (float) TEX_SIZE / tex->height));
	out->push_back(glm::vec2(sx + (float) TEX_SIZE / tex->width, sy + (float) TEX_SIZE / tex->width));
	out->push_back(glm::vec2(sx + (float) TEX_SIZE / tex->width, sy + (float) TEX_SIZE / tex->width));
	out->push_back(glm::vec2(sx + (float) TEX_SIZE / tex->width, sy));
	out->push_back(glm::vec2(sx, sy));
}


void wonder::wall::get_UV(std::vector<glm::vec2> * out) {
	out->push_back(glm::vec2(top_sx, top_sy));
	out->push_back(glm::vec2(top_sx, top_sy + (float) TEX_SIZE / tex->height));
	out->push_back(glm::vec2(top_sx + (float) TEX_SIZE / tex->width, top_sy + (float) TEX_SIZE / tex->width));
	out->push_back(glm::vec2(top_sx + (float) TEX_SIZE / tex->width, top_sy + (float) TEX_SIZE / tex->width));
	out->push_back(glm::vec2(top_sx + (float) TEX_SIZE / tex->width, top_sy));
	out->push_back(glm::vec2(top_sx, top_sy));
}

wonder::tile_set::~tile_set() {
	for (std::unordered_map<std::string, tile*>::iterator it = tiles.begin(); it != tiles.end(); ++it) {
		delete it->second;
	}
	tiles.clear(); // delete the pointers to the tiles.
	wonder::gl_c->delete_texture(tex);
}


void wonder::ground_tile_wrapper::get_shader_information(int x, int y, chunk* ch,
		std::vector<glm::vec3>* vBuf_out,
		std::vector<glm::vec2>* tBuf_out,
		std::vector<glm::vec3>* nBuf_out) {

	tile->get_UV(tBuf_out);

	vBuf_out->push_back(ch->get_relative_pos(x  ,y  ));
	vBuf_out->push_back(ch->get_relative_pos(x  ,y+1));
	vBuf_out->push_back(ch->get_relative_pos(x+1,y+1));
	vBuf_out->push_back(ch->get_relative_pos(x+1,y+1));
	vBuf_out->push_back(ch->get_relative_pos(x+1,y  ));
	vBuf_out->push_back(ch->get_relative_pos(x  ,y  ));

	nBuf_out->push_back(ch->get_normal(x,y));
	nBuf_out->push_back(ch->get_normal(x,y+1));
	nBuf_out->push_back(ch->get_normal(x+1,y+1));
	nBuf_out->push_back(ch->get_normal(x+1,y+1));
	nBuf_out->push_back(ch->get_normal(x+1,y));
	nBuf_out->push_back(ch->get_normal(x,y));
}

glm::vec3 normal(const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3) {
	glm::vec3 edge1 = v3 - v1;
	glm::vec3 edge2 = v2 - v1;
	return glm::cross(edge1, edge2);
}

#include "../../tools/console.h"
void wonder::ground_tile_wrapper::get_surface_normal_sum(int x, int y, chunk* ch) {

	glm::vec3 sw = ch->get_pos(x  ,y  );
	glm::vec3 se = ch->get_pos(x+1,y  );
	glm::vec3 nw = ch->get_pos(x  ,y+1);
	glm::vec3 ne = ch->get_pos(x+1,y+1);

	// case ne
	ch->set_normal(x+1,y+1,ch->get_normal(x+1,y+1)+normal(ne, se, nw));

	// case se
	ch->set_normal(x  ,y+1,ch->get_normal(x+1,y)+normal(se, sw, ne));

	// case sw
	ch->set_normal(x  ,y  ,ch->get_normal(x,y+1)+normal(sw, nw, se));

	// case nw
	ch->set_normal(x  ,y+1,ch->get_normal(x,y+1)+normal(nw, ne, sw));

}
