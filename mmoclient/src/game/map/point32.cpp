/*
 * intPoint.cpp
 *
 *  Created on: Feb 2, 2015
 *      Author: Dylan
 */
#include "point32.h"
#include <iostream>
wonder::point32::point32(int32_t x, int32_t y) {
	this->x = x;
	this->y = y;
}

int32_t wonder::point32::get_x() {
	return x;
}

int32_t wonder::point32::get_y() {
	return y;
}

bool wonder::point32::operator==(const point32& p) const {
	return x == p.x && y == p.y;
}

std::size_t wonder::point32_hasher::operator()(const point32& p) const {
	uint64_t key = ((uint64_t) p.y) + ((uint64_t) p.x) << 32;
	// Take the nearby points and make them far apart
	key = (~key) + (key << 18);
	key = key ^ (key >> 31);
	key = key * 21;
	key = key ^ (key >> 11);
	key = key + (key << 6);
	key = key ^ (key >> 22);
	// Scramble it up some more, weed out the collisions with some magic numbers
	key *= p.x * 1327144003;
	key ^= ~key * p.y * 486187739;
	return key;
}

