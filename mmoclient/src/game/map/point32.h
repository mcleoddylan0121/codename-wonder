/*
 * point32.h
 *
 *  Created on: Feb 2, 2015
 *      Author: Dylan
 */

#ifndef POINT32_H_
#define POINT32_H_
#include <cstdint>
#include "stdint.h"

namespace wonder {
	/*
	 * Integer valued point to be used
	 * for mapping to chunks, and defining their
	 * behavior
	 */
	class point32 {
	public:
		int32_t x, y;
		int32_t get_x();
		int32_t get_y();
		point32(int x, int y);
		virtual ~point32() {

		}
		int32_t get_id() const {
			return x * 100 + y;
		}
		bool operator==(const point32& other) const;
	};
	/*
	 * To be used with unordered maps, to map with chunks.
	 * In fact, that's point32's sole purpose for existing
	 */
	struct point32_hasher {
	public:
		std::size_t operator()(const point32& a) const;
	};
}

#endif /* INTPOINT_H_ */
