/*
 * wondermap.h
 *
 *  Created on: Feb 1, 2015
 *      Author: Dylan
 */

#ifndef GAMEMAP_H_
#define GAMEMAP_H_
#include <unordered_map>
#include <vector>
#include <time.h>
#include <cstdint>
#include <glm/glm.hpp>

#include "../../wonder.h"
#include "../../tools/simplex.h"
#include "../../tools/shape.h"
#include "point32.h"

namespace wonder {
	class tile; class tile_wrapper; class tile_set;
	class image; class texture;
	class directional_light;
	/*
	 * A collection of tiles. A chunk not only holds tiles,
	 * but also the entities contained within.
	 *
	 * since chunks extend point32, they map to themselves
	 * with map.get_chunk(point32 p)
	 *
	 * Note that this should NEVER be smaller than an entity.
	 */
	class chunk: public point32 {
	private:
		float delete_counter = 0;
		float* heights;
		glm::vec3* surface_normals; // The normals at ground level, doesn't include walls
		/* Shared references to the contained tiles */
		tile_wrapper** tiles;
	public:
//#define DRAW_NORMALS
#ifdef DRAW_NORMALS
		image* debug_model;
#endif

		image* model;
		void load_model(texture* tex);

		const static unsigned char SIZE = 40;
		chunk(int x, int y);
		/* Doesn't delete the tiles contained within. Why would we do that? */
		~chunk();
		void tick();
		void refresh();
		void render() const;
		void render_shadowmap() const;
		bool remove() const;

		void set_tile(int x, int y, tile_wrapper* t);

		tile_wrapper* operator()(int x, int y);

		float get_height(int x, int y);
		void set_height(int x, int y, float val);

		glm::vec3 get_normal(int x, int y);
		void set_normal(int x, int y, glm::vec3 val);

		glm::vec3 get_pos(int x, int y); // gives the xyz vector at that tile
		glm::vec3 get_relative_pos(int x, int y); // gives xyz vector relative to chunk at that tile
	};

	class map {
	private:
		/* The unordered map of chunks.
		 * Don't worry, the red squiggly lines are a stylistic choice.
		 */
		typedef std::unordered_map<point32, chunk*, point32_hasher> chunk_map;
		chunk_map chunks;

		float get_height(float x, float y);

		tile_set* tileset;
		//noise_2d* simp = new scaled_simplex(clock(),50.f);
		noise_2d* simp = new octave_simplex(4000, 0.6f, time(0));
		/* The powerhouse of the map class. Loads a chunk, then returns it */
		virtual chunk* create_chunk(const point32& p);
		void load_chunk(const point32& p);

		void refresh_radius(const point32& p, unsigned char radius);
		void load_radius(const point32& p, unsigned char radius);
		bool is_loaded(const point32& p);

		tile_wrapper* get_tile(float x, float y);

		static const int CHUNKS_PER_TICK = 1; 	// how many chunks we generate per tick, max
		int chunks_generated_this_tick = 0; 	// how many we've generated so far

	public:
		//only load chunks in this radius
		static const int LOAD_RADIUS = 6;
		static const int RENDER_RADIUS = 4;

		map(const std::string& tset);
		/* the center of the rendering/updates */
		point* focus;

		/* not const, because this may call create_chunk */
		chunk* operator ()(const point32& p);
		chunk* operator ()(float x, float y);

		void load_radius(float x, float y, unsigned char radius);
		bool is_loaded(float x, float y) const;

		void delete_chunk(float x, float y);

		virtual ~map();

		void render();
		void render_shadowmap();

		void move_focus(float x, float y);
		void tick();

		point32 to_map_coords(float x, float y);
		point32 to_map_coords(const point& p);

		chunk_map::iterator begin();
		chunk_map::iterator end();
	};

	/*
	 * Handles the generation through default client side generation,
	 * while the server sends only key chunks
	 */
	class world_map: public map {

	};

	/*
	 * Generation handled completely server sided; they must not
	 * know our secrets
	 */
	class instance_map: public map {

	};
}
#endif /* GAMEMAP_H_ */
