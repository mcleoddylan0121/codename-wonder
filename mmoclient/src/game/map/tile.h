/*
 * tile.h
 *
 *  Created on: Feb 22, 2015
 *      Author: Dylan
 */

#ifndef TILE_H_
#define TILE_H_
#include "../../wonder.h"
#include <glm/glm.hpp>
#include <vector>
#include <unordered_map>
namespace wonder {
	class chunk; // sorry
	class texture; class image;
	enum {N,NW,W,SW,S,SE,E,NE};

	class tile {
	public:
		texture* tex; // Including the texture isn't particularly useful, but we have no reason not to

		tile(texture* tex);
		virtual ~tile();

		static constexpr float SIZE = 32.f;
		static const unsigned char TEX_SIZE = 32;

		/* returns the texcoords represented by this tile */
		virtual void get_UV(std::vector<glm::vec2>* out) = 0;
	};

	class ground_tile: public tile {
	private:
		float sx, sy;
		public:
		ground_tile(uint16_t sx, uint16_t sy, texture* tex);
		~ground_tile();
		void get_UV(std::vector<glm::vec2>* out);
	};

	class wall: public tile {
	private:
		float side_sx, side_sy, top_sx, top_sy;
		public:
		wall(uint16_t side_sx, uint16_t side_sy, uint16_t top_sx, uint16_t top_sy, texture* tex);
		~wall();

		void get_UV(std::vector<glm::vec2>* out);
	};

	class tile_set {
	private:
		std::unordered_map<std::string, tile*> tiles;
		void load_tile(const std::string& name, tile* t);
		public:
		texture* tex;
		~tile_set();
		tile_set(const std::string& json);
		tile* operator [](const std::string& name);
	};

	class tile_wrapper {
	public:
		virtual ~tile_wrapper();

		virtual void get_shader_information(int x, int y, chunk* ch,
				std::vector<glm::vec3>* vBuf_out,
				std::vector<glm::vec2>* tBuf_out,
				std::vector<glm::vec3>* nBuf_out) = 0;
		virtual void get_surface_normal_sum(int x, int y, chunk* ch) = 0;
	};

	class ground_tile_wrapper: public tile_wrapper {
	public:
		ground_tile* tile;
		ground_tile_wrapper(ground_tile* t);

		~ground_tile_wrapper();

		void get_shader_information(int x, int y, chunk* ch,
				std::vector<glm::vec3>* vBuf_out,
				std::vector<glm::vec2>* tBuf_out,
				std::vector<glm::vec3>* nBuf_out);
		void get_surface_normal_sum(int x, int y, chunk* ch);
	};

	class wall_wrapper: public tile_wrapper {
	public:
		wall* w;
		float height;
		wall_wrapper(wall* w, float height);

		void get_shader_information(int x, int y, chunk* ch,
				std::vector<glm::vec3>* vBuf_out,
				std::vector<glm::vec2>* tBuf_out,
				std::vector<glm::vec3>* nBuf_out);
		void get_surface_normal_sum(int x, int y, chunk* ch);
	};
}

#endif /* TILE_H_ */
