/*
 * Map.cpp
 *
 *  Created on: Feb 2, 2015
 *      Author: Dylan
 */
#include "map.h"
#include "../../tools/console.h"
#include "../../display/image.h"
#include "../instance.h"
#include "tile.h"
#include "../../display/image.h"
#include "../../display/gl_controller.h"

#include <iostream>
#include <cmath>

wonder::chunk::chunk(int x, int y) :
		point32(x, y) {
	this->tiles = new tile_wrapper*[(SIZE+3)*(SIZE+3)];
	heights = new float[(SIZE + 3) * (SIZE + 3)];
	surface_normals = new glm::vec3[(SIZE + 3) * (SIZE + 3)];
	for(int i = 0; i < (SIZE+3) * (SIZE+3); i++)
		surface_normals[i] = glm::vec3(0.f,0.f,0.f);
	delete_counter = 0;
	model = nullptr; // lol warnings
}

wonder::chunk::~chunk() {
	for(int i = 0; i<(SIZE+3)*(SIZE+3); i++)
		delete tiles[i];
	delete[] tiles;
	delete model;
	delete[] heights;
	delete[] surface_normals;
#ifdef DRAW_NORMALS
	delete debug_model;
#endif
}

bool wonder::chunk::remove() const { return delete_counter > 2.0;}
void wonder::chunk::refresh() { delete_counter = 0;}

void wonder::chunk::tick() {
	delete_counter += wonder::delta;
}

wonder::tile_wrapper* wonder::chunk::operator()(int x, int y) { return tiles[(x + 1) + (y + 1) * (SIZE + 3)]; }
void wonder::chunk::set_tile(int x, int y, tile_wrapper* t) { tiles[(x + 1) + (y + 1) * (SIZE + 3)] = t; }

float wonder::chunk::get_height(int x, int y) { return heights[(x + 1) + (y + 1) * (SIZE + 3)]; }
void wonder::chunk::set_height(int x, int y, float val) { heights[(x + 1) + (y + 1) * (SIZE + 3)] = val; }

glm::vec3 wonder::chunk::get_normal(int x, int y) { return surface_normals[(x + 1) + (y + 1) * (SIZE + 3)]; }
void wonder::chunk::set_normal(int x, int y, glm::vec3 val) { surface_normals[(x + 1) + (y + 1) * (SIZE + 3)] = val; }

glm::vec3 wonder::chunk::get_pos(int x, int y) { return glm::vec3((float) (this->x * SIZE + x) * tile::SIZE, (float) (this->y * SIZE + y) * tile::SIZE, get_height(x,y));}
glm::vec3 wonder::chunk::get_relative_pos(int x, int y) { return glm::vec3((float) x * tile::SIZE, (float) y * tile::SIZE, get_height(x,y)); }

wonder::chunk* wonder::map::operator ()(const point32& p) {
	wonder::chunk* ch = chunks[p];
	if (!ch)
		ch = create_chunk(p);
	return ch;
}

wonder::chunk* wonder::map::operator ()(float x, float y) {
	return (*this)(to_map_coords(x, y));
}

void wonder::map::delete_chunk(float x, float y) {
	chunk* ch = (*this)(x, y);
	chunks.erase(*ch);
	delete ch;
}

float wonder::map::get_height(float x, float y) {
	//return fmax(8000 * (*simp)(x, y),0);
	return pow(2, 3.5f * (*simp)(x, y) + 1) * 500;
}

void wonder::map::load_chunk(const point32& p) {
	if (!is_loaded(p) && (chunks_generated_this_tick < CHUNKS_PER_TICK)) {
		create_chunk(p);
		chunks_generated_this_tick++;
	}
}

void wonder::chunk::load_model(texture* tex) {
	std::vector<glm::vec3> vBuf;
	std::vector<glm::vec2> tBuf;
	std::vector<glm::vec3> nBuf;

	for(int x = 0; x < SIZE; x++)
		for(int y = 0; y < SIZE; y++)
			(*this)(x,y)->get_shader_information(x,y,this,&vBuf,&tBuf,&nBuf);
	model = tex->load_model(vBuf, tBuf, nBuf, GL_TRIANGLES);
}

wonder::chunk* wonder::map::create_chunk(const point32& p) {
	wonder::chunk* ch = new wonder::chunk(p.x,p.y);

	//Generate all of the base tiles first, then get to the hard part.
	for (int x = -1; x <= chunk::SIZE + 1; x++)
		for (int y = -1; y <= chunk::SIZE + 1; y++) {
			ch->set_height(x,y,get_height(p.x * chunk::SIZE + x, p.y * chunk::SIZE + y));

			ch->set_tile  (x,y,get_tile  (p.x * chunk::SIZE + x, p.y * chunk::SIZE + y));
		}
	for (int x = -1; x <= chunk::SIZE; x++)
		for (int y = -1; y <= chunk::SIZE; y++)
			(*ch)(x,y)->get_surface_normal_sum(x, y, ch);
	for (int x = -1; x < chunk::SIZE + 1; x++)
		for (int y = -1; y < chunk::SIZE + 1; y++)
			ch->set_normal(x,y,glm::normalize(ch->get_normal(x,y)));
	ch->load_model(tileset->tex);
#ifdef DRAW_NORMALS
	/* BEGIN DEBUG */

	// drawing normals
	vBuf.clear();// this is the only thing that should even be here
	nBuf.clear();// im using normals and i don't know why
	tBuf.clear();// really don't feel like adding support for colors, so we'll just use a texture .-.
	for (int x = 1; x <= chunk::SIZE; x++) {
		for (int y = 1; y <= chunk::SIZE; y++) {
			glm::vec3 pos = glm::vec3((float) x * tile::SIZE, (float) y * tile::SIZE, heights[x + y * (chunk::SIZE + 3)]);
			vBuf.push_back(pos);
			glm::vec3 n = surface_normals[x + y * (chunk::SIZE + 3)];
			vBuf.push_back(pos + n * 20.f);
			tBuf.push_back(glm::vec2(256.f,0.f));
			tBuf.push_back(glm::vec2(288.f,32.f));
			nBuf.push_back(n);
			nBuf.push_back(n);
		}
	}
	ch->debug_model = tileset->tex->load_model(vBuf, tBuf, nBuf, GL_LINES);

	/* END DEBUG */
#endif

	chunks[p] = ch;
	return ch;
}

wonder::tile_wrapper* wonder::map::get_tile(float x, float y) {
	float height = (*simp)(x, y);
	std::string name;
	if	   (height > 0.7)
		name = "snow";
	else if(height > 0.55)
		name = "rocks";
	else if (height > 0.4)
		name = "dirt";
	else if (height > 0.2)
		name = "tall grass";
	else if (height > 0.0)
		name = "grass";
	else if (height > -0.2)
		name = "water";
	else
		name = "deep water";
	ground_tile* t = (ground_tile*) (*tileset)[name];
	return new ground_tile_wrapper(t);
}

wonder::map::chunk_map::iterator wonder::map::begin() {
	return chunks.begin();
}

wonder::map::chunk_map::iterator wonder::map::end() {
	return chunks.end();
}

wonder::map::map(const std::string& tset) {
	focus = new point(0, 0);
	tileset = new tile_set(tset);
}

wonder::map::~map() {
	delete tileset;
	delete focus;
	for (chunk_map::iterator it = chunks.begin(); it != chunks.end(); ++it) {
		delete (it->second);
	}
	chunks.clear();
}

void wonder::map::render() {
	point32 p = to_map_coords(*focus);
	for (int x = p.x - RENDER_RADIUS; x <= p.x + RENDER_RADIUS; x++)
		for (int y = p.y - RENDER_RADIUS; y <= p.y + RENDER_RADIUS; y++) {
			// I'm not sure if we want to check if it's loaded
			if (is_loaded(point32(x, y))) {
				chunk* ch = (*this)(point32(x, y));
				ch->render();
			}
		}
}

void wonder::map::render_shadowmap() {
	point32 p = to_map_coords(*focus);
		for (int x = p.x - RENDER_RADIUS - 2; x <= p.x + RENDER_RADIUS + 2; x++)
			for (int y = p.y - RENDER_RADIUS - 2; y <= p.y + RENDER_RADIUS + 2; y++) {
				// I'm not sure if we want to check if it's loaded
				if (is_loaded(point32(x, y))) {
					chunk* ch = (*this)(point32(x, y));
					ch->render_shadowmap();
				}
			}
}

void wonder::chunk::render() const {
	model->render(x * SIZE * tile::SIZE, y * SIZE * tile::SIZE, 0.f,  1.f,
			1.f, 1.f);

#ifdef DRAW_NORMALS
	debug_model->render(x * SIZE * tile::SIZE, (float) y * SIZE * tile::SIZE, 0.f,  1.f,
			1.f, 1.f);
#endif
}

void wonder::chunk::render_shadowmap() const {
	model->render_shadowmap(
			x * SIZE * tile::SIZE, y * SIZE * tile::SIZE, 0.f,
			1.f, 1.f, 1.f);
}

bool wonder::map::is_loaded(const point32& p) {
	return chunks.count(p) != 0;
}

void wonder::map::load_radius(float x, float y, unsigned char radius) {
	load_radius(to_map_coords(x, y), radius);
}

void wonder::map::refresh_radius(const wonder::point32& p, unsigned char radius) {
	for (int x = p.x - radius; x <= p.x + radius; x++)
		for (int y = p.y - radius; y <= p.y + radius; y++)
			if (is_loaded(point32(x, y))) {
				chunk* ch = (*this)(point32(x, y));
				ch->refresh();
			}
}

void wonder::map::load_radius(const wonder::point32& p, unsigned char radius) {
	// we start from the center and spiral outward
	for (int rad = 0; rad <= radius; rad++) {
		// load north and south borders
		for (int x = p.x - rad; x <= p.x + rad; x++) {
			// load north
			load_chunk(point32(x, p.y + rad));
			// load south
			load_chunk(point32(x, p.y - rad));
		}
		// load east and west
		for (int y = p.y - rad; y <= p.y + rad; y++) {
			// load east
			load_chunk(point32(p.x + rad, y));
			// load west
			load_chunk(point32(p.x - rad, y));
		}
	}
}

void wonder::map::move_focus(float x, float y) {
	focus->move(x * wonder::delta, y * wonder::delta);
}

wonder::point32 wonder::map::to_map_coords(float x, float y) {
	return point32(
			(int32_t) floor(x / (wonder::chunk::SIZE * wonder::tile::SIZE)),
			(int32_t) floor(y / (wonder::chunk::SIZE * wonder::tile::SIZE)));
}

wonder::point32 wonder::map::to_map_coords(const point& p) {
	return to_map_coords(p.x, p.y);
}

void wonder::map::tick() {
	chunks_generated_this_tick = 0;
	point32 p = to_map_coords(*focus);
	load_radius(p, LOAD_RADIUS);
	refresh_radius(p, LOAD_RADIUS);

	chunk_map::iterator it = chunks.begin();
	while (it != chunks.end()) {
		chunk* ch = it->second;
		ch->tick();
		if (ch->remove()) {
			delete it->second;
			chunks.erase(it++);
		} else {
			++it;
		}
	}
}

