/*
 * instance.cpp
 *
 *  Created on: Feb 1, 2015
 *      Author: Dylan
 */
#include "instance.h"
#include "map/map.h"
#include "../io/keyboard.h"
#include "SDL.h"
#include <iostream>
#include "../display/gl_controller.h"
#include "map/tile.h"
#include "../tools/shape.h"
#include "../display/camera.h"
#include <cmath>

#include <gl/glew.h>
#include <gl/gl.h>
#include "../display/light.h"

void wonder::instance::render(frame_buffer* to) {
	wonder::gl_c->bind_camera(cam);
	wonder::gl_c->bind_directional_light(sun);
	if(!temptick) {
		glCullFace(GL_FRONT);
		glDisable(GL_CULL_FACE);
		sun->cam->set_position(focus->x, focus->y, 0.f);
		sun->cam->set_camera(glm::vec3(sin(time), 3.f , cos(time * 1.61803398875f) + 1.23456789f ));
		wonder::gl_c->bind_program(directional_light_shader);
		wonder::gl_c->bind_frame_buffer(sun->fbo);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		current_map->render_shadowmap();
	}
	temptick = (temptick + 1) % 10;
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	wonder::gl_c->bind_program(default_program);
	wonder::gl_c->bind_frame_buffer(to);
	current_map->render(); // render map first
	//entities.render(); // render entities second
}

wonder::instance::instance() {
	focus = new point(0.f, 0.f);
	cam = new camera(
			glm::ortho(-scale,   scale,
					   -scale,   scale,
					   z_magic, -z_magic),
			glm::vec3(0.f, 0.f, 0.f),
			glm::vec3(0.f, 1.f, 1.f),
			glm::vec3(0.f, 0.f, 1.f));

	sun = new directional_light(
						4096, 4096,
new camera( glm::ortho<float>(
					   -12000,   12000,
					   -12000,   12000,
					   -15000.f, 15000.f),
			glm::vec3(0.f, 0.f, 0.f),
			glm::vec3(1.f, 0.f, 1.f),
			glm::vec3(0.f, 0.f, 1.f))
			);
	camera_height = 1.f;
	current_map = new map("tiles");
	set_focus(0, 0);
	default_program = ((gl_33_controller*)wonder::gl_c)->temp;
}

void wonder::instance::tick() {
	/* laziness: */
	if (wonder::keys(SDLK_LSHIFT))
		zoom(-1000);
	if (wonder::keys(SDLK_LCTRL))
		zoom(1000);
	if (wonder::keys(SDLK_s))
		move_focus(-10 * s * cos(rotation), -10 * s * sin(rotation));
	if (wonder::keys(SDLK_a))
		move_focus(10 * s * sin(rotation), -10 * s * cos(rotation));
	if (wonder::keys(SDLK_w))
		move_focus(10 * s * cos(rotation), 10 * s * sin(rotation));
	if (wonder::keys(SDLK_d))
		move_focus(-10 * s * sin(rotation), 10 * s * cos(rotation));
	if (wonder::keys(SDLK_q))
		rotate_camera(-1.4f);
	if (wonder::keys(SDLK_e))
		rotate_camera(1.4f);
	if (wonder::keys(SDLK_v))
		s *= 1.01;
	if (wonder::keys(SDLK_b))
		s *= 0.99;
	if (wonder::keys(SDLK_x))
		std::cout << "position: (" << focus->x << ", " << focus->y << ")" << std::endl;
	if (wonder::keys(SDLK_DOWN))
		move_camera_height(-1.13f);
	if (wonder::keys(SDLK_UP))
		move_camera_height(1.13f);
	chunk* center = (*current_map)(focus->x,focus->y);
	int x = ((int)floor(focus->x / tile::SIZE)) % (int)chunk::SIZE;
	int y = ((int)floor(focus->y / tile::SIZE)) % (int)chunk::SIZE;
	if(x<0) x+= chunk::SIZE;
	if(y<0) y+= chunk::SIZE;
	time += wonder::delta / 5.f;
	cam->set_position(focus->x, focus->y, center->get_height(x,y));
	cam->set_camera(glm::vec3(cos(rotation), sin(rotation), -camera_height));
	cam->set_projection(glm::ortho(
			-scale,   scale,
			-scale,   scale,
			 z_magic,-z_magic));
	cam->recalculate();
	current_map->tick();
}

void wonder::instance::rotate_camera(float rad_per_sec) {
	rotation += rad_per_sec * wonder::delta;
}

void wonder::instance::set_focus(float x, float y) {
	focus->x = x;
	focus->y = y;
	update_focus();
}

void wonder::instance::move_focus(float x, float y) {
	focus->x += x * wonder::delta;
	focus->y += y * wonder::delta;
	update_focus();
}

void wonder::instance::update_focus() {
	current_map->focus = focus;
}

void wonder::instance::zoom(float per_sec) {
	this->scale += wonder::delta * per_sec;
}

void wonder::instance::move_camera_height(float per_sec) {
	camera_height += wonder::delta * per_sec;
}

wonder::instance::~instance() {
	delete current_map;
	delete cam;
}

