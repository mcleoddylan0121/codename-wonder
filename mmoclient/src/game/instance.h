/*
 * wonder.h
 *
 *  Created on: Feb 1, 2015
 *      Author: Dylan
 */

#ifndef INSTANCE_H_
#define INSTANCE_H_

#include "../display/scene.h"
#include "entities/player.h"
namespace wonder {
	class camera;
	class map;
	class program; class frame_buffer;
	class point;
	class directional_light;
	class instance: public scene {
	private:
		point* focus;
		float scale = 1600.f;
		float z_magic = 100000.f;
		float rotation = 0;
		float camera_height = 1;
		float time = 0;
	public:
		int temptick = 0;
		directional_light* sun;
		program* default_program;
		camera* cam;
		void rotate_camera(float rad_per_sec);
		float s = 60;
		map* current_map;
		//player player;
		void tick();
		instance();
		void render(frame_buffer* to);
		~instance();
		void set_focus(float x, float y);
		void move_focus(float x, float y);
		void update_focus();

		void zoom(float per_sec);
		void move_camera_height(float per_sec);
	};
}




#endif /* INSTANCE_H_ */
