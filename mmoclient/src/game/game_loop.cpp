/*
 * game_loop.cpp
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */
#include "game_loop.h"
#include "../wonder.h"
#include "../tools/console.h"
#include "../display/scene.h"
#include "instance.h"
#include <string>
#include <sstream>
#include "map/map.h"
#include "../io/keyboard.h"

#include "../display/sdl_controller.h"
#include "../display/gl_controller.h"

#include <iostream>

std::string ctos(clock_t i) {
	std::stringstream ss;
	ss << i;
	return ss.str();
}

wonder::game_loop::game_loop() {
	wonder::println("Creating Window...");
	wonder::sdl_c = new wonder::sdl_controller(1024, 768, false);

	wonder::println("Initializing OpenGL...");
	wonder::gl_c = new wonder::gl_33_controller();

	wonder::println("Starting Loop...");
	wonder::running = true;
	_scene = new instance(); //
	get_delta();
}

void wonder::game_loop::loop() {
	get_delta();
	int nb_frames = 0;
	double delta_sum = 0.f;
	std::cout << "Number of tiles: "
			<< (2 * map::RENDER_RADIUS + 1) * (2 * map::RENDER_RADIUS + 1) * chunk::SIZE
					* chunk::SIZE << std::endl;
	while (wonder::running) {
		nb_frames++;
		get_delta();
		delta_sum += wonder::delta;
		if (delta_sum > 1.0f) {
			std::cout << "FPS: " << nb_frames << std::endl;
			std::cout << "Avg Tick Time: " << 1.0f / nb_frames << std::endl;
			nb_frames = 0;
			delta_sum--;
		}
		if(wonder::delta > 0.03)
			std::cout << "[WARNING] Delta-t: " << wonder::delta << std::endl;
		// Events
		wonder::sdl_c->handle_events();


		// TODO: tick
		//std::cout << "DELTA: " << wonder::delta << std::endl;
		_scene->tick();

		// Render
		wonder::gl_c->render(_scene);
		//wonder::sdl_c->sleep(200); // TODO: temporary
		wonder::sdl_c->swap_buffer();
		if (wonder::keys(SDLK_ESCAPE))
			wonder::loop->kill();
	}
}

wonder::game_loop::~game_loop() {
	delete _scene;
	wonder::println("Deleting OpenGL...");
	delete wonder::gl_c;
	wonder::println("Deleting SDL...");
	delete wonder::sdl_c;
}

void wonder::game_loop::get_delta() {
	float time = get_time();
	wonder::delta = time - last_frame;
	last_frame = time;
}

float wonder::game_loop::get_time() {
	return ((float) clock()) / CLOCKS_PER_SEC;
}

void wonder::game_loop::kill() {
	wonder::println("Closing...");
	wonder::running = false;
}

