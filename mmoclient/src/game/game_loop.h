/*
 * game_loop.h
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */

#ifndef GAME_LOOP_H_
#define GAME_LOOP_H_
#include "../display/scene.h"
#include <time.h>
namespace wonder {
	class game_loop {
	private:
	public:
		float last_frame;
		scene* _scene;
		game_loop();
		void loop();
		virtual ~game_loop();
		void kill();
		void get_delta();
		/* returns in ms */
		float get_time();
	};
}

#endif /* GAME_LOOP_H_ */
