/*
 * io.h
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */

#ifndef IO_H_
#define IO_H_
#include <string>

namespace wonder {
	std::string load_text_file(std::string path);
	GLuint load_texture(std::string path, unsigned int* width, unsigned int* height);
	GLuint load_shaders(std::string vertex_shader, std::string fragment_shader);
}

#endif /* IO_H_ */
