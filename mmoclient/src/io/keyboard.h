/*
 * keyboard.h
 *
 *  Created on: Feb 22, 2015
 *      Author: Dylan
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_
#include <map>
namespace wonder {
	class keyboard {
		std::map<int,bool>* keys;
	public:
		keyboard();
		~keyboard();
		void set_key(int key_code, bool pressed);
		bool get_key(int key_code);
		bool operator ()(int key_code);
	};
}

#endif /* KEYBOARD_H_ */
