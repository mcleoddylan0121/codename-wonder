/*
 * io.cpp
 *
 *  Created on: Feb 7, 2015
 *      Author: Dylan
 */
#include <GL/glew.h>
#include <GL/gl.h>
#include <SOIL.h>
#include <fstream>

#include "io.h"
#include "../wonder.h"
#include "../tools/console.h"
#include <iostream>

std::string wonder::load_text_file(std::string path) {
	std::ifstream fin;							// Create a file in stream
	fin.open((wonder::ASSETS + path).c_str());	// Open the file

	if (!fin.is_open()) { 						// If there was an error opening the file
		wonder::eprintf("Error loading file: %s\n\tCannot access the file.\n", path.c_str());
		return "";
	}

	std::string temp;							// Store the data in here
	std::string line;							// Temporary variable to store lines

	while (!fin.eof()) {						// While we haven't reached the end of the file
		std::getline(fin, line);				// Get the next line in the file and store it in line
		temp.append(line + "\n");				// Append line to our data
	}

	fin.close();					// Close the stream so the file can be opened by other programs

	return temp;
}

GLuint wonder::load_shaders(std::string vertex_shader, std::string fragment_shader) {
	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertex_shaderStr = wonder::load_text_file(vertex_shader);	// Read the text of our vertex shader
	std::string fragment_shaderStr = wonder::load_text_file(fragment_shader);	// Read the text of our fragment shader
	const char * vertex_shaderSrc = vertex_shaderStr.c_str();// We have to create a temporary string before this
	const char * fragment_shaderSrc = fragment_shaderStr.c_str();// because if we don't, the vertex_shader and fragment_shader data would be destroyed
	// and we'd have an empty char array
	GLint result = GL_FALSE;
	int loglength;

	// Compile vertex shader
	wonder::printf("Compiling vertex shader: %s...\n", vertex_shader.c_str());
	glShaderSource(vertShader, 1, &vertex_shaderSrc, 0);
	glCompileShader(vertShader);

	// Check for errors in vertex shader
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &loglength);
	if (loglength > 1) {
		char error[loglength];
		glGetShaderInfoLog(vertShader, loglength, 0, &error[0]);
		wonder::eprintln(error);
	}

	// Compile fragment shader
	wonder::printf("Compiling fragment shader: %s...\n", fragment_shader.c_str());
	glShaderSource(fragShader, 1, &fragment_shaderSrc, 0);
	glCompileShader(fragShader);

	// Check for errors in fragment shader
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &loglength);
	if (loglength > 1) {
		char error[loglength];
		glGetShaderInfoLog(fragShader, loglength, 0, &error[0]);
		wonder::eprintln(error);
	}

	// Link the shaders into a program
	wonder::println("Linking...");
	GLuint program = glCreateProgram();
	glAttachShader(program, vertShader);
	glAttachShader(program, fragShader);
	glLinkProgram(program);

	// Check for errors in the linkage
	glGetProgramiv(program, GL_COMPILE_STATUS, &result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &loglength);
	if (loglength > 1) {
		char error[loglength];
		glGetProgramInfoLog(program, loglength, 0, &error[0]);
		wonder::eprintln(error);
	}

	// Clean up shaders
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	return program;
}

