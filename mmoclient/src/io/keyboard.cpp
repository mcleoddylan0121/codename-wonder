/*
 * keyboard.cpp
 *
 *  Created on: Feb 22, 2015
 *      Author: Dylan
 */
#include "keyboard.h"
wonder::keyboard::keyboard() {
	keys = new std::map<int, bool>();
}
wonder::keyboard::~keyboard() {
	delete keys;
}
void wonder::keyboard::set_key(int key_code, bool pressed) {
	(*keys)[key_code] = pressed;
}

bool wonder::keyboard::get_key(int key_code) 	 { return (*keys)[key_code]; }
bool wonder::keyboard::operator ()(int key_code) { return get_key(key_code); }

