/*
 * Main.cpp
 *
 *  Created on: Feb 1, 2015
 *      Author: Dylan
 */
#include "wonder.h"
#include "io/keyboard.h"
#include "game/game_loop.h"
#include "display/light.h"

namespace wonder {
	sdl_controller* sdl_c;
	gl_controller* gl_c;
	game_loop* loop;
	program* directional_light_shader;
	keyboard keys;
	float delta; // in seconds
	bool running = false;
}

int main(int argc, char **argv) {
	wonder::loop = new wonder::game_loop();
	while (wonder::running)
		wonder::loop->loop();
	delete wonder::loop;
	return 0;
}
